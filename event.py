import re
import sys
def extract(file):
    with open(file) as f:
        print( list(set(re.findall( 'event[=]["]([^"]+)["]', f.read() ))))
if __name__ == "__main__":
    extract(sys.argv[1])


