#!/usr/bin/env python3
from __future__ import annotations
import os
import sys

PRG_PATH = "tmc2fcs_generator" # should be included in path 
TPL_SIM_ROOT = "../tmc2fcs_templates/templates/per_component"
TPL_CL_ROOT = "../tmc2fcs_templates/templates/clientwrapper/per_component/"
INC_ROOT = "../tmc2fcs_templates/includes"

cmd_args = {
  "piezo": "-d Piezo -s ./piezo/piezo.scxml.xml -t ./piezo/Piezo.tmc -I ./piezo",
  "shutter":"-d Shutter -s ./shutter/shutter.scxml.xml -t ./shutter/Shutter.tmc -I ./shutter",
  "lamp":"-d Lamp -s ./lamp/lamp.scxml.xml -t ./lamp/Lamp.tmc -I ./lamp",
  "motor":"-d Motor -s ./motor/motor.scxml.xml -t ./motor/Motor.tmc -I ./motor -e MAIN.Motor001 -p E_MOTOR_",
  "actuator":"-d Actuator -s ./actuator/actuator.scxml.xml -t ./actuator/Actuator.tmc -I ./actuator",
  "iodev":"-d Iodev -s ./iodev/iodev.scxml.xml -t ./iodev/IODev.tmc -I ./iodev -e MAIN.IODev002 ",
  "drot":"-d Drot -s ./drot/drot.scxml.xml -t ./motor/Motor.tmc -I ./motor -I ./ma -I ./drot -e MAIN.drot -p E_MA_",
  "adc":"-d Adc -s ./adc/adc.scxml.xml -t ./motor/Motor.tmc -I ./motor -I ./ma -I ./adc -e MAIN.adc -p E_MA_",

}


if __name__ == "__main__":
    directive = sys.argv[1]
    if directive == "sim":
        TPL_ROOT = TPL_SIM_ROOT
    elif directive == "client":
        TPL_ROOT = TPL_CL_ROOT
    else:
        print(f"Error , unknown directive {directive}. Must be 'sim' or 'client'")
        print("Usage: ./export.py DIRECTIVE OUTPUDIR DEVICE")
        raise ValueError("Wrong directive")

    output = sys.argv[2]
    for device in sys.argv[3:]:
        
        dev_args = cmd_args[device]
        cmd = f"""{PRG_PATH} -o {output} -n fcfsim --ProjName=framework --ProjPrefix=ifw --tpl={TPL_ROOT} -I {INC_ROOT} {dev_args} """ 
        print(cmd)
        os.system(cmd)

